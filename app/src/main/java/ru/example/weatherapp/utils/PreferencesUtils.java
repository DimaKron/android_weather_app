package ru.example.weatherapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

public class PreferencesUtils {

    private static final String PREFERENCES_NAME = "main";

    private static final String KEY_WEATHER_LAST_UPDATE = "weather_last_update";

    public static final long DEFAULT_LAST_UPDATE = Long.MIN_VALUE;

    public static void setWeatherLastUpdate(@NonNull Context context, long timeInMillis){
        editor(context).putLong(KEY_WEATHER_LAST_UPDATE, timeInMillis).apply();
    }

    public static long getWeatherLastUpdate(@NonNull Context context){
        return prefs(context).getLong(KEY_WEATHER_LAST_UPDATE, DEFAULT_LAST_UPDATE);
    }

    @NonNull
    private static SharedPreferences prefs(@NonNull Context context){
        return context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    @NonNull
    private static SharedPreferences.Editor editor(@NonNull Context context){
        return prefs(context).edit();
    }

}
