package ru.example.weatherapp.utils;

import android.support.annotation.NonNull;

import java.text.SimpleDateFormat;

public class DateTimeUtils {

    public static final String PATTERN_TIME_UI = "d MMM yyyy, HH:mm";

    @NonNull
    public static String format(long millis, @NonNull String pattern){
        return new SimpleDateFormat(pattern).format(millis);
    }

}
