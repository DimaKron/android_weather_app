package ru.example.weatherapp.utils;

public class TempUtils {

    public static double covertKelToCel(double kel) {
        return kel - 273.15;
    }
}
