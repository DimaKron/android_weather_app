package ru.example.weatherapp.database;

import android.support.annotation.NonNull;

public class DatabaseFactory {

    private static DatabaseProvider provider;

    @NonNull
    public static DatabaseProvider getProvider() {
        return provider;
    }

    public static void setProvider(@NonNull DatabaseProvider provider) {
        DatabaseFactory.provider = provider;
    }
}
