package ru.example.weatherapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.util.List;

import ru.example.weatherapp.model.entity.ForecastDate;
import ru.example.weatherapp.model.entity.WeatherCity;
import ru.example.weatherapp.model.table.CityWeatherTable;
import ru.example.weatherapp.model.table.DateWeatherTable;
import ru.example.weatherapp.model.wrapper.ContentValuesWrapper;
import ru.example.weatherapp.model.wrapper.WeatherCityCursorWrapper;
import ru.example.weatherapp.model.wrapper.WeatherDateCursorWrapper;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;


public class DatabaseProvider {

    private BriteDatabase database;

    private ContentValuesWrapper contentValuesWrapper;

    public DatabaseProvider(@NonNull Context context) {
        database = createDatabase(context);
        contentValuesWrapper = new ContentValuesWrapper();
    }

    @NonNull
    private BriteDatabase createDatabase(@NonNull Context context){
        DatabaseHelper helper = new DatabaseHelper(context);
        SqlBrite sqlBrite = new SqlBrite.Builder().build();
        return sqlBrite.wrapDatabaseHelper(helper, Schedulers.io());
    }

    @NonNull
    public Observable<Long> provideCityWeatherInsertQuery(@NonNull List<WeatherCity> cities){
        return Observable.from(cities)
                .map(new Func1<WeatherCity, Long>() {
                    @Override
                    public Long call(WeatherCity city) {
                        return database.insert(CityWeatherTable.NAME, contentValuesWrapper.wrapCityWeather(city), SQLiteDatabase.CONFLICT_REPLACE);
                    }
                });
    }

    @NonNull
    public Observable<List<WeatherCity>> provideCityWeatherSelectQuery(){
        return database.createQuery(CityWeatherTable.NAME, CityWeatherTable.getSelectQuery())
                .map(new Func1<SqlBrite.Query, List<WeatherCity>>() {
                    @Override
                    public List<WeatherCity> call(SqlBrite.Query query) {
                        return new WeatherCityCursorWrapper(query.run()).getWeatherCities();
                    }
                });
    }

    @NonNull
    public Observable<Long> provideCityWeatherLastForecastUpdateQuery(long cityId){
        return Observable.just(cityId)
                .map(new Func1<Long, Long>() {
                    @Override
                    public Long call(Long aLong) {
                        long lastUpdateTime = System.currentTimeMillis();
                        database.update(CityWeatherTable.NAME, contentValuesWrapper.wrapCityWeatherLastForecastUpdate(lastUpdateTime), CityWeatherTable.getWhereClause(aLong));
                        return lastUpdateTime;
                    }
                });
    }


    @NonNull
    public Observable<Long> provideTableClearQuery(@NonNull String tableName){
        return Observable.just(tableName)
                .map(new Func1<String, Long>() {
                    @Override
                    public Long call(String s) {
                        return (long)database.delete(s, null);
                    }
                });
    }

    @NonNull
    public Observable<Long> provideDateWeatherInsertQuery(@NonNull List<ForecastDate> dates, final long cityId){
        return Observable.from(dates)
                .map(new Func1<ForecastDate, Long>() {
                    @Override
                    public Long call(ForecastDate date) {
                        date.setCityId(cityId);
                        return database.insert(DateWeatherTable.NAME, contentValuesWrapper.wrapDateForecast(date), SQLiteDatabase.CONFLICT_REPLACE);
                    }
                });
    }

    @NonNull
    public Observable<Long> provideDateWeatherCleanInsertQuery(@NonNull List<ForecastDate> dates, final long cityId){
        return Observable.concat(
                provideTableClearQuery(DateWeatherTable.NAME),
                provideDateWeatherInsertQuery(dates, cityId));
    }

    @NonNull
    public Observable<List<ForecastDate>> provideDateWeatherSelectQuery(long cityId){
        return database.createQuery(DateWeatherTable.NAME, DateWeatherTable.getSelectQuery(cityId))
                .map(new Func1<SqlBrite.Query, List<ForecastDate>>() {
                    @Override
                    public List<ForecastDate> call(SqlBrite.Query query) {
                        return new WeatherDateCursorWrapper(query.run()).getWeatherDates();
                    }
                });
    }
}
