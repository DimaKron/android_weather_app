package ru.example.weatherapp.model.entity;

import com.google.gson.annotations.SerializedName;

public class ForecastWeather {

    public static final String STATE_SNOW = "Snow";

    public static final String STATE_CLOUDS = "Clouds";

    public static final String STATE_CLEAR = "Clear";

    @SerializedName("main")
    private String main;

    public ForecastWeather(){}

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

}
