package ru.example.weatherapp.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ForecastResponse {

    @SerializedName("list")
    private List<ForecastDate> dates;

    public List<ForecastDate> getDates() {
        return dates;
    }

    public void setDates(List<ForecastDate> dates) {
        this.dates = dates;
    }
}
