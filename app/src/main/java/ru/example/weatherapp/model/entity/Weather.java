package ru.example.weatherapp.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Weather implements Parcelable{

    public static final String STATE_SNOW = "Snow";

    public static final String STATE_CLOUDS = "Clouds";

    public static final String STATE_CLEAR = "Clear";

    @SerializedName("main")
    private String main;

    public Weather(){}

    protected Weather(Parcel in) {
        main = in.readString();
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(main);
    }

    public static final Creator<Weather> CREATOR = new Creator<Weather>() {
        @Override
        public Weather createFromParcel(Parcel in) {
            return new Weather(in);
        }

        @Override
        public Weather[] newArray(int size) {
            return new Weather[size];
        }
    };

}
