package ru.example.weatherapp.model.wrapper;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import ru.example.weatherapp.model.entity.ForecastClouds;
import ru.example.weatherapp.model.entity.ForecastDate;
import ru.example.weatherapp.model.entity.ForecastMain;
import ru.example.weatherapp.model.entity.ForecastWeather;
import ru.example.weatherapp.model.entity.ForecastWind;
import ru.example.weatherapp.model.table.DateWeatherTable;


public class WeatherDateCursorWrapper extends CursorWrapper {

    public WeatherDateCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    @NonNull
    public List<ForecastDate> getWeatherDates(){
        List<ForecastDate> result = new ArrayList<>();

        moveToFirst();
        while (!isBeforeFirst() && !isAfterLast()){
            ForecastDate date = getWeatherDate();

            if(date != null) result.add(date);

            moveToNext();
        }

        return result;
    }

    @Nullable
    private ForecastDate getWeatherDate(){
        if (isAfterLast() || isBeforeFirst()) return null;

        ForecastDate result = new ForecastDate();
        result.setDate(getString(getColumnIndex(DateWeatherTable.COLUMN_DATE)));
        result.setCityId(getLong(getColumnIndex(DateWeatherTable.COLUMN_CITY_ID)));

        ForecastMain main = new ForecastMain();
        main.setTemp(getDouble(getColumnIndex(DateWeatherTable.COLUMN_TEMP)));
        main.setHumidity(getDouble(getColumnIndex(DateWeatherTable.COLUMN_HUMIDITY)));
        main.setPressure(getDouble(getColumnIndex(DateWeatherTable.COLUMN_PRESSURE)));
        result.setMain(main);

        ForecastClouds clouds = new ForecastClouds();
        clouds.setAll(getDouble(getColumnIndex(DateWeatherTable.COLUMN_CLOUDS)));
        result.setClouds(clouds);

        ForecastWind wind = new ForecastWind();
        wind.setSpeed(getDouble(getColumnIndex(DateWeatherTable.COLUMN_WIND_SPEED)));
        result.setWind(wind);

        ForecastWeather weather = new ForecastWeather();
        weather.setMain(getString(getColumnIndex(DateWeatherTable.COLUMN_WEATHER_STATE)));
        result.setWeather(weather);

        return result;
    }
}
