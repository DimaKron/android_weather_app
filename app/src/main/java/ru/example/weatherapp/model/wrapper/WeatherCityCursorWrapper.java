package ru.example.weatherapp.model.wrapper;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import ru.example.weatherapp.model.entity.Weather;
import ru.example.weatherapp.model.entity.WeatherCity;
import ru.example.weatherapp.model.entity.WeatherClouds;
import ru.example.weatherapp.model.entity.WeatherMain;
import ru.example.weatherapp.model.entity.WeatherWind;
import ru.example.weatherapp.model.table.CityWeatherTable;
import ru.example.weatherapp.view_presenter.main.CityWeatherViewHolder;


public class WeatherCityCursorWrapper extends CursorWrapper {

    public WeatherCityCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    @NonNull
    public List<WeatherCity> getWeatherCities(){
        List<WeatherCity> result = new ArrayList<>();

        moveToFirst();
        while (!isBeforeFirst() && !isAfterLast()){
            WeatherCity city = getWeatherCity();

            if(city != null) result.add(city);

            moveToNext();
        }

        return result;
    }

    @Nullable
    private WeatherCity getWeatherCity(){
        if (isAfterLast() || isBeforeFirst()) return null;

        WeatherCity result = new WeatherCity();
        result.setName(getString(getColumnIndex(CityWeatherTable.COLUMN_NAME)));
        result.setId(getLong(getColumnIndex(CityWeatherTable.COLUMN_ID)));
        result.setLastForecastUpdate(getLong(getColumnIndex(CityWeatherTable.COLUMN_LAST_FORECAST_UPDATE)));

        WeatherMain main = new WeatherMain();
        main.setTemp(getDouble(getColumnIndex(CityWeatherTable.COLUMN_TEMP)));
        main.setHumidity(getDouble(getColumnIndex(CityWeatherTable.COLUMN_HUMIDITY)));
        main.setPressure(getDouble(getColumnIndex(CityWeatherTable.COLUMN_PRESSURE)));
        result.setMain(main);

        WeatherClouds clouds = new WeatherClouds();
        clouds.setAll(getDouble(getColumnIndex(CityWeatherTable.COLUMN_CLOUDS)));
        result.setClouds(clouds);

        WeatherWind wind = new WeatherWind();
        wind.setSpeed(getDouble(getColumnIndex(CityWeatherTable.COLUMN_WIND_SPEED)));
        result.setWind(wind);

        Weather weather = new Weather();
        weather.setMain(getString(getColumnIndex(CityWeatherTable.COLUMN_WEATHER_STATE)));
        result.setWeather(weather);

        return result;
    }
}
