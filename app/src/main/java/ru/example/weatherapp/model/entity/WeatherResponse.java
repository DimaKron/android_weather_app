package ru.example.weatherapp.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherResponse {

    @SerializedName("list")
    private List<WeatherCity> cities;

    public List<WeatherCity> getCities() {
        return cities;
    }

    public void setCities(List<WeatherCity> cities) {
        this.cities = cities;
    }
}
