package ru.example.weatherapp.model.table;

import android.support.annotation.NonNull;

public class CityWeatherTable {

    public static final String NAME = "city_weather";

    public static final String COLUMN_ID = "_id";

    public static final String COLUMN_NAME = "name";

    public static final String COLUMN_TEMP = "temp";

    public static final String COLUMN_PRESSURE = "pressure";

    public static final String COLUMN_HUMIDITY = "humidity";

    public static final String COLUMN_WIND_SPEED = "wind_speed";

    public static final String COLUMN_CLOUDS = "clouds";

    public static final String COLUMN_WEATHER_STATE = "weather_state";

    public static final String COLUMN_LAST_FORECAST_UPDATE = "last_forecast_update";

    @NonNull
    public static String getCreateQuery(){
        return "CREATE TABLE " + NAME + " (" + COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY," +
                COLUMN_NAME + " TEXT NOT NULL," +
                COLUMN_TEMP + " REAL," +
                COLUMN_PRESSURE + " REAL," +
                COLUMN_HUMIDITY + " REAL," +
                COLUMN_WIND_SPEED + " REAL," +
                COLUMN_CLOUDS + " REAL," +
                COLUMN_LAST_FORECAST_UPDATE + " INTEGER," +
                COLUMN_WEATHER_STATE + " TEXT);";
    }

    @NonNull
    public static String getSelectQuery(){
        return "SELECT * FROM " + NAME + ";";
    }

    public static String getWhereClause(long id){
        return COLUMN_ID + "=" + id;
    }

}
