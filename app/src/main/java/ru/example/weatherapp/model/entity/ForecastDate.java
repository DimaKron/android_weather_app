package ru.example.weatherapp.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ForecastDate {

    @SerializedName("dt_txt")
    private String date;

    @SerializedName("main")
    private ForecastMain main;

    @SerializedName("weather")
    private List<ForecastWeather> weather;

    @SerializedName("clouds")
    private ForecastClouds clouds;

    @SerializedName("wind")
    private ForecastWind wind;

    private long cityId;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ForecastMain getMain() {
        return main;
    }

    public void setMain(ForecastMain main) {
        this.main = main;
    }

    public void setWeather(ForecastWeather weather) {
        this.weather = new ArrayList<>();
        this.weather.add(weather);
    }

    public ForecastWeather getWeather() {
        return weather == null || weather.isEmpty()? null: weather.get(0);
    }

    public ForecastClouds getClouds() {
        return clouds;
    }

    public void setClouds(ForecastClouds clouds) {
        this.clouds = clouds;
    }

    public ForecastWind getWind() {
        return wind;
    }

    public void setWind(ForecastWind wind) {
        this.wind = wind;
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }
}
