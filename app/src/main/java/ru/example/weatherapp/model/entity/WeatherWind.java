package ru.example.weatherapp.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class WeatherWind implements Parcelable {

    @SerializedName("speed")
    private double speed;

    public WeatherWind(){}

    protected WeatherWind(Parcel in) {
        speed = in.readDouble();
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(speed);
    }

    public static final Creator<WeatherWind> CREATOR = new Creator<WeatherWind>() {
        @Override
        public WeatherWind createFromParcel(Parcel in) {
            return new WeatherWind(in);
        }

        @Override
        public WeatherWind[] newArray(int size) {
            return new WeatherWind[size];
        }
    };
}
