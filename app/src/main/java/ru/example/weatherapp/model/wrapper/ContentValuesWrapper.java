package ru.example.weatherapp.model.wrapper;

import android.content.ContentValues;
import android.support.annotation.NonNull;

import ru.example.weatherapp.model.entity.ForecastDate;
import ru.example.weatherapp.model.entity.WeatherCity;
import ru.example.weatherapp.model.table.CityWeatherTable;
import ru.example.weatherapp.model.table.DateWeatherTable;

public class ContentValuesWrapper {

    public ContentValuesWrapper(){}

    @NonNull
    public ContentValues wrapCityWeather(@NonNull WeatherCity city){
        ContentValues result = new ContentValues();
        result.put(CityWeatherTable.COLUMN_ID, city.getId());
        result.put(CityWeatherTable.COLUMN_NAME, city.getName());
        result.put(CityWeatherTable.COLUMN_TEMP, city.getMain().getTemp());
        result.put(CityWeatherTable.COLUMN_HUMIDITY, city.getMain().getHumidity());
        result.put(CityWeatherTable.COLUMN_PRESSURE, city.getMain().getPressure());
        result.put(CityWeatherTable.COLUMN_CLOUDS, city.getClouds().getAll());
        result.put(CityWeatherTable.COLUMN_WEATHER_STATE, city.getWeather().getMain());
        result.put(CityWeatherTable.COLUMN_WIND_SPEED, city.getWind().getSpeed());
        result.put(CityWeatherTable.COLUMN_LAST_FORECAST_UPDATE, city.getLastForecastUpdate());
        return result;
    }

    @NonNull
    public ContentValues wrapCityWeatherLastForecastUpdate(long lastUpdateMillis){
        ContentValues result = new ContentValues();
        result.put(CityWeatherTable.COLUMN_LAST_FORECAST_UPDATE, lastUpdateMillis);
        return result;
    }

    @NonNull
    public ContentValues wrapDateForecast(@NonNull ForecastDate date){
        ContentValues result = new ContentValues();
        result.put(DateWeatherTable.COLUMN_DATE, date.getDate());
        result.put(DateWeatherTable.COLUMN_CITY_ID, date.getCityId());
        result.put(DateWeatherTable.COLUMN_TEMP, date.getMain().getTemp());
        result.put(DateWeatherTable.COLUMN_HUMIDITY, date.getMain().getHumidity());
        result.put(DateWeatherTable.COLUMN_PRESSURE, date.getMain().getPressure());
        result.put(DateWeatherTable.COLUMN_CLOUDS, date.getClouds().getAll());
        result.put(DateWeatherTable.COLUMN_WEATHER_STATE, date.getWeather().getMain());
        result.put(DateWeatherTable.COLUMN_WIND_SPEED, date.getWind().getSpeed());
        return result;
    }
}
