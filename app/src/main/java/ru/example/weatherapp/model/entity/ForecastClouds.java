package ru.example.weatherapp.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ForecastClouds{

    @SerializedName("all")
    private double all;

    public ForecastClouds(){}

    public double getAll() {
        return all;
    }

    public void setAll(double all) {
        this.all = all;
    }
}
