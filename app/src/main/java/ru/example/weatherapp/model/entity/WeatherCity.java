package ru.example.weatherapp.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class WeatherCity implements Parcelable{

    public static final long DEFAULT_LAST_FORECAST_UPDATE = Long.MIN_VALUE;

    @SerializedName("id")
    private long id;

    @SerializedName("name")
    private String name;

    @SerializedName("main")
    private WeatherMain main;

    @SerializedName("wind")
    private WeatherWind wind;

    @SerializedName("clouds")
    private WeatherClouds clouds;

    @SerializedName("weather")
    private List<Weather> weather;

    private long lastForecastUpdate = DEFAULT_LAST_FORECAST_UPDATE;

    public WeatherCity(){}

    protected WeatherCity(Parcel in) {
        id = in.readLong();
        name = in.readString();
        main = in.readParcelable(WeatherMain.class.getClassLoader());
        wind = in.readParcelable(WeatherWind.class.getClassLoader());
        clouds = in.readParcelable(WeatherClouds.class.getClassLoader());
        weather = in.createTypedArrayList(Weather.CREATOR);
        lastForecastUpdate = in.readLong();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WeatherMain getMain() {
        return main;
    }

    public void setMain(WeatherMain main) {
        this.main = main;
    }

    public WeatherWind getWind() {
        return wind;
    }

    public void setWind(WeatherWind wind) {
        this.wind = wind;
    }

    public WeatherClouds getClouds() {
        return clouds;
    }

    public void setClouds(WeatherClouds clouds) {
        this.clouds = clouds;
    }

    public void setWeather(Weather weather) {
        this.weather = new ArrayList<>();
        this.weather.add(weather);
    }

    public Weather getWeather() {
        return weather == null || weather.isEmpty()? null: weather.get(0);
    }

    public long getLastForecastUpdate() {
        return lastForecastUpdate;
    }

    public void setLastForecastUpdate(long lastForecastUpdate) {
        this.lastForecastUpdate = lastForecastUpdate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeParcelable(main, flags);
        dest.writeParcelable(wind, flags);
        dest.writeParcelable(clouds, flags);
        dest.writeTypedList(weather);
        dest.writeLong(lastForecastUpdate);
    }

    public static final Creator<WeatherCity> CREATOR = new Creator<WeatherCity>() {
        @Override
        public WeatherCity createFromParcel(Parcel in) {
            return new WeatherCity(in);
        }

        @Override
        public WeatherCity[] newArray(int size) {
            return new WeatherCity[size];
        }
    };
}
