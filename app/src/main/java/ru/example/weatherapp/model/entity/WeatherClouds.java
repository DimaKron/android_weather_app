package ru.example.weatherapp.model.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class WeatherClouds implements Parcelable{

    @SerializedName("all")
    private double all;

    public WeatherClouds(){}

    protected WeatherClouds(Parcel in) {
        all = in.readDouble();
    }

    public double getAll() {
        return all;
    }

    public void setAll(double all) {
        this.all = all;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(all);
    }

    public static final Creator<WeatherClouds> CREATOR = new Creator<WeatherClouds>() {
        @Override
        public WeatherClouds createFromParcel(Parcel in) {
            return new WeatherClouds(in);
        }

        @Override
        public WeatherClouds[] newArray(int size) {
            return new WeatherClouds[size];
        }
    };
}
