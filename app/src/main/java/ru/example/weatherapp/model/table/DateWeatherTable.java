package ru.example.weatherapp.model.table;

import android.support.annotation.NonNull;

public class DateWeatherTable {

    public static final String NAME = "date_weather";

    public static final String COLUMN_DATE = "date";

    public static final String COLUMN_CITY_ID = "city_id";

    public static final String COLUMN_TEMP = "temp";

    public static final String COLUMN_PRESSURE = "pressure";

    public static final String COLUMN_HUMIDITY = "humidity";

    public static final String COLUMN_WIND_SPEED = "wind_speed";

    public static final String COLUMN_CLOUDS = "clouds";

    public static final String COLUMN_WEATHER_STATE = "weather_state";

    @NonNull
    public static String getCreateQuery(){
        return "CREATE TABLE " + NAME + " (" + COLUMN_DATE + " TEXT NOT NULL," +
                COLUMN_CITY_ID + " INTEGER NOT NULL," +
                COLUMN_TEMP + " REAL," +
                COLUMN_PRESSURE + " REAL," +
                COLUMN_HUMIDITY + " REAL," +
                COLUMN_WIND_SPEED + " REAL," +
                COLUMN_CLOUDS + " REAL," +
                COLUMN_WEATHER_STATE + " TEXT," +
                " PRIMARY KEY (" + COLUMN_DATE + "," + COLUMN_CITY_ID + "));";
    }

    @NonNull
    public static String getSelectQuery(long cityId){
        return "SELECT * FROM " + NAME + " WHERE " + COLUMN_CITY_ID + "=" + cityId + ";";
    }

}
