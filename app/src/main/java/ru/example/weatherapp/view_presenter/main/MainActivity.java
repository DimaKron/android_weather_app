package ru.example.weatherapp.view_presenter.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.tatarka.rxloader.RxLoaderManager;
import ru.example.weatherapp.R;
import ru.example.weatherapp.model.entity.WeatherCity;
import ru.example.weatherapp.utils.DateTimeUtils;
import ru.example.weatherapp.utils.PreferencesUtils;
import ru.example.weatherapp.view_presenter.base.BaseActivity;
import ru.example.weatherapp.view_presenter.base.OnRecyclerViewItemClickListener;
import ru.example.weatherapp.view_presenter.forecast.ForecastActivity;

public class MainActivity extends BaseActivity implements MainView, SwipeRefreshLayout.OnRefreshListener, OnRecyclerViewItemClickListener {

    private static final int REQUEST_CODE_FORECAST = 100;

    @BindView(R.id.text_view_last_update) TextView lastUpdateTextView;

    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    @BindView(R.id.frame_layout_progress) FrameLayout progressLayout;

    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout refreshLayout;

    private MainPresenter presenter;

    private MainAdapter recyclerAdapter;

    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        unbinder = ButterKnife.bind(this);

        refreshLayout.setOnRefreshListener(this);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerAdapter = new MainAdapter(this);
        recyclerView.setAdapter(recyclerAdapter);

        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));

        refreshLastUpdate();

        presenter = new MainPresenter(RxLoaderManager.get(this), this);
        presenter.loadWeather(getResources().getStringArray(R.array.city_ids));
        if (PreferencesUtils.getWeatherLastUpdate(this) != PreferencesUtils.DEFAULT_LAST_UPDATE){
            presenter.loadLocalWeather();
        }
    }

    @Override
    protected int getContentViewRes() {
        return R.layout.activity_main;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_refresh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_refresh:
                presenter.loadWeather(getResources().getStringArray(R.array.city_ids));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onWeatherLoadingStarted() {
        if (PreferencesUtils.getWeatherLastUpdate(this) == PreferencesUtils.DEFAULT_LAST_UPDATE) {
            progressLayout.setVisibility(View.VISIBLE);
        } else {
            refreshLayout.setRefreshing(true);
        }
    }

    @Override
    public void onWeatherLoadingSuccess() {
        presenter.loadLocalWeather();

        PreferencesUtils.setWeatherLastUpdate(this, System.currentTimeMillis());
        refreshLastUpdate();

        progressLayout.setVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onLocalWeatherLoadingSuccess(@NonNull List<WeatherCity> cities) {
        recyclerAdapter.setData(cities);
    }

    @Override
    public void onRefresh() {
        presenter.loadWeather(getResources().getStringArray(R.array.city_ids));
    }

    @Override
    public void onRecyclerViewItemClick(int position, @ActionType int action) {
        switch (action){
            case ACTION_MAIN:
                recyclerAdapter.onItemClicked(position);
                break;
            case ACTION_1:
                startActivityForResult(ForecastActivity.makeIntent(this, recyclerAdapter.getData().get(position), position), REQUEST_CODE_FORECAST);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            switch (requestCode){
                case REQUEST_CODE_FORECAST:
                    int position = data.getIntExtra(ForecastActivity.KEY_POSITION, ForecastActivity.DEFAULT_POSITION);
                    long lastUpdateMillis = data.getLongExtra(ForecastActivity.KEY_LAST_FORECAST_UPDATE, 0);
                    recyclerAdapter.getData().get(position).setLastForecastUpdate(lastUpdateMillis);
                    break;
            }
        }
    }

    private void refreshLastUpdate(){
        long lastUpdate = PreferencesUtils.getWeatherLastUpdate(this);
        if(lastUpdate == PreferencesUtils.DEFAULT_LAST_UPDATE){
            lastUpdateTextView.setVisibility(View.GONE);
        } else {
            lastUpdateTextView.setVisibility(View.VISIBLE);
            lastUpdateTextView.setText(DateTimeUtils.format(lastUpdate, DateTimeUtils.PATTERN_TIME_UI));
        }
    }
}
