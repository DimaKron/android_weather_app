package ru.example.weatherapp.view_presenter.forecast;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.example.weatherapp.R;
import ru.example.weatherapp.model.entity.ForecastDate;
import ru.example.weatherapp.model.entity.Weather;
import ru.example.weatherapp.utils.DateTimeUtils;
import ru.example.weatherapp.utils.TempUtils;
import ru.example.weatherapp.view_presenter.base.OnRecyclerViewItemClickListener;


public class DateWeatherViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text_view_temp) TextView tempTextView;

    @BindView(R.id.text_view_date) TextView dateTextView;

    @BindView(R.id.image_view_state) ImageView stateImageView;

    @BindView(R.id.linear_layout_stats) LinearLayout statsLinearLayout;

    @BindView(R.id.text_view_pressure) TextView pressureTextView;

    @BindView(R.id.text_view_humidity) TextView humidityTextView;

    @BindView(R.id.text_view_clouds) TextView cloudsTextView;

    @BindView(R.id.text_view_wind) TextView windTextView;

    private OnRecyclerViewItemClickListener itemClickListener;

    public DateWeatherViewHolder(View itemView, @Nullable OnRecyclerViewItemClickListener itemClickListener) {
        super(itemView);
        this.itemClickListener = itemClickListener;

        ButterKnife.bind(this, itemView);
    }

    public void bind(@NonNull ForecastDate date, boolean isExpanded){
        dateTextView.setText(date.getDate());
        tempTextView.setText(itemView.getContext().getString(R.string.main_temp, Math.round(TempUtils.covertKelToCel(date.getMain().getTemp()))));

        switch (date.getWeather().getMain()){
            case Weather.STATE_CLEAR:
                stateImageView.setImageResource(R.drawable.ic_sunny);
                break;
            case Weather.STATE_CLOUDS:
                stateImageView.setImageResource(R.drawable.ic_cloud);
                break;
            case Weather.STATE_SNOW:
                stateImageView.setImageResource(R.drawable.ic_snowflake);
                break;
            default:
                stateImageView.setImageResource(R.drawable.ic_info);
        }

        if(isExpanded){
            statsLinearLayout.setVisibility(View.VISIBLE);
            pressureTextView.setText(itemView.getContext().getString(R.string.main_pressure, Math.round(date.getMain().getPressure())));
            humidityTextView.setText(itemView.getContext().getString(R.string.main_humidity, Math.round(date.getMain().getHumidity())));
            cloudsTextView.setText(itemView.getContext().getString(R.string.main_clouds, Math.round(date.getClouds().getAll())));
            windTextView.setText(itemView.getContext().getString(R.string.main_wind, Math.round(date.getWind().getSpeed())));
        } else {
            statsLinearLayout.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.relative_layout_root)
    public void onItemClick(){
        if(itemClickListener!=null){
            itemClickListener.onRecyclerViewItemClick(getAdapterPosition(), OnRecyclerViewItemClickListener.ACTION_MAIN);
        }
    }
}
