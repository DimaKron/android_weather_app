package ru.example.weatherapp.view_presenter.main;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.example.weatherapp.R;
import ru.example.weatherapp.model.entity.WeatherCity;
import ru.example.weatherapp.view_presenter.base.OnRecyclerViewItemClickListener;

public class MainAdapter extends RecyclerView.Adapter<CityWeatherViewHolder>{

    private List<WeatherCity> data;

    private boolean[] isElementExpanded;

    private OnRecyclerViewItemClickListener clickListener;

    public MainAdapter(@Nullable OnRecyclerViewItemClickListener clickListener){
        isElementExpanded = new boolean[0];
        data = new ArrayList<>();
        this.clickListener = clickListener;
    }

    @Override
    public CityWeatherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city_weather, parent, false);
        return new CityWeatherViewHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(CityWeatherViewHolder holder, int position) {
        holder.bind(data.get(position), isElementExpanded[position]);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(@NonNull List<WeatherCity> data) {
        this.data = data;
        isElementExpanded = new boolean[data.size()];
        notifyDataSetChanged();
    }

    @NonNull
    public List<WeatherCity> getData() {
        return data;
    }

    // тут можно поработать напрямую с вьюшкой, но в целях экономии времени я воздержался
    public void onItemClicked(int position){
        isElementExpanded[position] = !isElementExpanded[position];
        notifyItemChanged(position);
    }
}
