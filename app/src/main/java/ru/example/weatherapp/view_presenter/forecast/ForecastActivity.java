package ru.example.weatherapp.view_presenter.forecast;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.tatarka.rxloader.RxLoaderManager;
import ru.example.weatherapp.R;
import ru.example.weatherapp.model.entity.ForecastDate;
import ru.example.weatherapp.model.entity.WeatherCity;
import ru.example.weatherapp.utils.DateTimeUtils;
import ru.example.weatherapp.utils.PreferencesUtils;
import ru.example.weatherapp.view_presenter.base.BaseActivity;
import ru.example.weatherapp.view_presenter.base.OnRecyclerViewItemClickListener;
import ru.example.weatherapp.view_presenter.main.MainAdapter;

public class ForecastActivity extends BaseActivity implements ForecastView, SwipeRefreshLayout.OnRefreshListener, OnRecyclerViewItemClickListener {

    private static final String KEY_WEATHER_CITY = "weather_city";

    public static final String KEY_LAST_FORECAST_UPDATE = "last_forecast_update";

    public static final String KEY_POSITION = "position";

    public static final int DEFAULT_POSITION = -1;

    @BindView(R.id.text_view_name) TextView nameTextView;

    @BindView(R.id.text_view_last_update) TextView lastUpdateTextView;

    @BindView(R.id.frame_layout_progress) FrameLayout progressLayout;

    @BindView(R.id.swipe_refresh_layout) SwipeRefreshLayout refreshLayout;

    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    private ForecastAdapter recyclerAdapter;

    private LinearLayoutManager layoutManager;

    private WeatherCity city;

    private int cityPosition;

    private ForecastPresenter presenter;

    @NonNull
    public static Intent makeIntent(@NonNull Context context, @NonNull WeatherCity city, int position){
        return new Intent(context, ForecastActivity.class)
                .putExtra(KEY_WEATHER_CITY, city)
                .putExtra(KEY_POSITION, position);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        city = getIntent().getParcelableExtra(KEY_WEATHER_CITY);

        cityPosition = getIntent().getIntExtra(KEY_POSITION, DEFAULT_POSITION);

        unbinder = ButterKnife.bind(this);

        nameTextView.setText(city.getName());

        refreshLayout.setOnRefreshListener(this);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        recyclerAdapter = new ForecastAdapter(this);
        recyclerView.setAdapter(recyclerAdapter);

        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));

        presenter = new ForecastPresenter(RxLoaderManager.get(this), this, city.getId());
        presenter.loadForecast();
        if (city.getLastForecastUpdate() != WeatherCity.DEFAULT_LAST_FORECAST_UPDATE){
            presenter.loadLocalForecast();
        }

        refreshLastUpdate();
    }

    @Override
    protected int getContentViewRes() {
        return R.layout.activity_forecast;
    }

    private void refreshLastUpdate(){
        if(city.getLastForecastUpdate() == WeatherCity.DEFAULT_LAST_FORECAST_UPDATE){
            lastUpdateTextView.setVisibility(View.GONE);
        } else {
            lastUpdateTextView.setVisibility(View.VISIBLE);
            lastUpdateTextView.setText(DateTimeUtils.format(city.getLastForecastUpdate(), DateTimeUtils.PATTERN_TIME_UI));
        }
    }

    @Override
    public void onForecastLoadingStarted() {
        if (city.getLastForecastUpdate() == WeatherCity.DEFAULT_LAST_FORECAST_UPDATE) {
            progressLayout.setVisibility(View.VISIBLE);
        } else {
            refreshLayout.setRefreshing(true);
        }
    }

    @Override
    public void onForecastLoadingSuccess(long lastUpdateMillis) {
        presenter.loadLocalForecast();

        Intent resultIntent = new Intent();
        resultIntent.putExtra(KEY_LAST_FORECAST_UPDATE, lastUpdateMillis);
        resultIntent.putExtra(KEY_POSITION, cityPosition);
        setResult(RESULT_OK, resultIntent);

        city.setLastForecastUpdate(lastUpdateMillis);
        refreshLastUpdate();

        progressLayout.setVisibility(View.GONE);
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void onLocalForecastLoadingSuccess(@NonNull List<ForecastDate> dates) {
        recyclerAdapter.setData(dates);
    }

    @Override
    public void onRefresh() {
        presenter.loadForecast();
    }

    @Override
    public void onRecyclerViewItemClick(int position, int action) {
        switch (action) {
            case ACTION_MAIN:
                recyclerAdapter.onItemClicked(position);
                break;
        }
    }
}
