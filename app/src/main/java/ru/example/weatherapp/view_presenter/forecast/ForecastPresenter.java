package ru.example.weatherapp.view_presenter.forecast;

import android.support.annotation.NonNull;

import java.util.List;

import me.tatarka.rxloader.RxLoader;
import me.tatarka.rxloader.RxLoaderManager;
import me.tatarka.rxloader.RxLoaderObserver;
import ru.example.weatherapp.BuildConfig;
import ru.example.weatherapp.api.ApiFactory;
import ru.example.weatherapp.database.DatabaseFactory;
import ru.example.weatherapp.model.entity.ForecastDate;
import ru.example.weatherapp.model.entity.ForecastResponse;
import ru.example.weatherapp.model.entity.WeatherCity;
import ru.example.weatherapp.utils.RxUtils;
import ru.example.weatherapp.view_presenter.base.LoaderTag;
import rx.Observable;
import rx.functions.Func1;

public class ForecastPresenter {

    private RxLoaderManager loaderManager;

    private ForecastView view;

    private RxLoader<Long> forecastLoader;

    private RxLoader<List<ForecastDate>> localForecastLoader;

    private long cityId;

    public ForecastPresenter(@NonNull RxLoaderManager loaderManager, @NonNull ForecastView view, long cityId) {
        this.loaderManager = loaderManager;
        this.view = view;
        this.cityId = cityId;
    }

    public void loadForecast(){
        forecastLoader = loaderManager.create(LoaderTag.FORECAST,
                ApiFactory.getApiProvider()
                        .provideWeatherService()
                        .loadForecast(BuildConfig.API_KEY, cityId)
                        .flatMap(new Func1<ForecastResponse, Observable<Long>>() {
                            @Override
                            public Observable<Long> call(ForecastResponse forecastResponse) {
                                return DatabaseFactory.getProvider()
                                        .provideDateWeatherCleanInsertQuery(forecastResponse.getDates(), cityId);
                            }
                        })
                        .toList()
                        .flatMap(new Func1<List<Long>, Observable<Long>>() {
                            @Override
                            public Observable<Long> call(List<Long> longs) {
                                return DatabaseFactory.getProvider().provideCityWeatherLastForecastUpdateQuery(cityId);
                            }
                        })
                        .compose(RxUtils.<Long>applySchedulers())
                        .retry(),
                new RxLoaderObserver<Long>() {

                    @Override
                    public void onStarted() {
                        view.onForecastLoadingStarted();
                    }

                    @Override
                    public void onNext(Long value) {
                        view.onForecastLoadingSuccess(value);
                    }

                    @Override
                    public void onCompleted() {
                        forecastLoader.clear();
                    }

                    @Override
                    public void onError(Throwable e) {
                        forecastLoader.clear();
                    }
                }).restart();
    }

    public void loadLocalForecast(){
        localForecastLoader = loaderManager.create(LoaderTag.FORECAST_LOCAL,
                DatabaseFactory.getProvider()
                        .provideDateWeatherSelectQuery(cityId)
                        .compose(RxUtils.<List<ForecastDate>>applySchedulers()),
                new RxLoaderObserver<List<ForecastDate>>() {

                    @Override
                    public void onNext(List<ForecastDate> dates) {
                        view.onLocalForecastLoadingSuccess(dates);
                        localForecastLoader.clear();
                    }

                    @Override
                    public void onError(Throwable e) {
                        localForecastLoader.clear();
                    }
                }).restart();
    }

}
