package ru.example.weatherapp.view_presenter.base;

public interface LoaderTag {

    // MAIN
    String MAIN_WEATHER = "main_weather";
    String MAIN_LOCAl_WEATHER = "main_local_weather";

    // FORECAST
    String FORECAST = "forecast";
    String FORECAST_LOCAL = "forecast_local";
}
