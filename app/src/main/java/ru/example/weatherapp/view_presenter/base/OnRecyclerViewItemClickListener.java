package ru.example.weatherapp.view_presenter.base;

import android.support.annotation.IntDef;

public interface OnRecyclerViewItemClickListener {

    int ACTION_MAIN = 0;

    int ACTION_1 = 1;

    int ACTION_2 = 2;

    @IntDef({ACTION_MAIN, ACTION_1, ACTION_2})
    @interface ActionType {}

    void onRecyclerViewItemClick(int position, @ActionType int action);

}
