package ru.example.weatherapp.view_presenter.forecast;

import android.support.annotation.NonNull;

import java.util.List;

import ru.example.weatherapp.model.entity.ForecastDate;

public interface ForecastView {

    void onForecastLoadingStarted();

    void onForecastLoadingSuccess(long lastUpdateMillis);

    void onLocalForecastLoadingSuccess(@NonNull List<ForecastDate> dates);

}
