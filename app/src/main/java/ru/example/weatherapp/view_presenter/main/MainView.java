package ru.example.weatherapp.view_presenter.main;

import android.support.annotation.NonNull;

import java.util.List;

import ru.example.weatherapp.model.entity.WeatherCity;

public interface MainView {

    void onWeatherLoadingStarted();

    void onWeatherLoadingSuccess();

    void onLocalWeatherLoadingSuccess(@NonNull List<WeatherCity> cities);
}
