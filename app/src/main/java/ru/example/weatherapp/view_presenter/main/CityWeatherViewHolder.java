package ru.example.weatherapp.view_presenter.main;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.example.weatherapp.R;
import ru.example.weatherapp.model.entity.Weather;
import ru.example.weatherapp.model.entity.WeatherCity;
import ru.example.weatherapp.utils.TempUtils;
import ru.example.weatherapp.view_presenter.base.OnRecyclerViewItemClickListener;


public class CityWeatherViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text_view_temp) TextView tempTextView;

    @BindView(R.id.text_view_name) TextView nameTextView;

    @BindView(R.id.image_view_state) ImageView stateImageView;

    @BindView(R.id.linear_layout_stats) LinearLayout statsLinearLayout;

    @BindView(R.id.text_view_pressure) TextView pressureTextView;

    @BindView(R.id.text_view_humidity) TextView humidityTextView;

    @BindView(R.id.text_view_clouds) TextView cloudsTextView;

    @BindView(R.id.text_view_wind) TextView windTextView;

    private OnRecyclerViewItemClickListener itemClickListener;

    public CityWeatherViewHolder(View itemView, @Nullable OnRecyclerViewItemClickListener itemClickListener) {
        super(itemView);
        this.itemClickListener = itemClickListener;

        ButterKnife.bind(this, itemView);
    }

    public void bind(@NonNull WeatherCity city, boolean isExpanded){
        nameTextView.setText(city.getName());
        tempTextView.setText(itemView.getContext().getString(R.string.main_temp, Math.round(TempUtils.covertKelToCel(city.getMain().getTemp()))));

        switch (city.getWeather().getMain()){
            case Weather.STATE_CLEAR:
                stateImageView.setImageResource(R.drawable.ic_sunny);
                break;
            case Weather.STATE_CLOUDS:
                stateImageView.setImageResource(R.drawable.ic_cloud);
                break;
            case Weather.STATE_SNOW:
                stateImageView.setImageResource(R.drawable.ic_snowflake);
                break;
            default:
                stateImageView.setImageResource(R.drawable.ic_info);
        }

        if(isExpanded){
            statsLinearLayout.setVisibility(View.VISIBLE);
            pressureTextView.setText(itemView.getContext().getString(R.string.main_pressure, Math.round(city.getMain().getPressure())));
            humidityTextView.setText(itemView.getContext().getString(R.string.main_humidity, Math.round(city.getMain().getHumidity())));
            cloudsTextView.setText(itemView.getContext().getString(R.string.main_clouds, Math.round(city.getClouds().getAll())));
            windTextView.setText(itemView.getContext().getString(R.string.main_wind, Math.round(city.getWind().getSpeed())));
        } else {
            statsLinearLayout.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.relative_layout_root)
    public void onItemClick(){
        if(itemClickListener!=null){
            itemClickListener.onRecyclerViewItemClick(getAdapterPosition(), OnRecyclerViewItemClickListener.ACTION_MAIN);
        }
    }

    @OnClick(R.id.button_forecast)
    public void onForecastClick(){
        if(itemClickListener!=null){
            itemClickListener.onRecyclerViewItemClick(getAdapterPosition(), OnRecyclerViewItemClickListener.ACTION_1);
        }
    }
}
