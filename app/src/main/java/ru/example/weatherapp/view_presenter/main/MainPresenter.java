package ru.example.weatherapp.view_presenter.main;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.List;

import me.tatarka.rxloader.RxLoader;
import me.tatarka.rxloader.RxLoaderManager;
import me.tatarka.rxloader.RxLoaderObserver;
import ru.example.weatherapp.BuildConfig;
import ru.example.weatherapp.api.ApiFactory;
import ru.example.weatherapp.database.DatabaseFactory;
import ru.example.weatherapp.model.entity.WeatherCity;
import ru.example.weatherapp.model.entity.WeatherResponse;
import ru.example.weatherapp.utils.RxUtils;
import ru.example.weatherapp.view_presenter.base.LoaderTag;
import rx.Observable;
import rx.functions.Func1;

public class MainPresenter {

    private RxLoaderManager loaderManager;

    private MainView view;

    private RxLoader<Long> weatherLoader;

    private RxLoader<List<WeatherCity>> localWeatherLoader;

    public MainPresenter(@NonNull RxLoaderManager loaderManager, @NonNull MainView view) {
        this.loaderManager = loaderManager;
        this.view = view;
    }

    public void loadWeather(String ... cityIds){
        weatherLoader = loaderManager.create(LoaderTag.MAIN_WEATHER,
                ApiFactory.getApiProvider()
                        .provideWeatherService()
                        .loadWeather(BuildConfig.API_KEY, TextUtils.join(",", cityIds))
                        .flatMap(new Func1<WeatherResponse, Observable<Long>>() {
                            @Override
                            public Observable<Long> call(WeatherResponse response) {
                                return DatabaseFactory.getProvider().provideCityWeatherInsertQuery(response.getCities());
                            }
                        })
                        .compose(RxUtils.<Long>applySchedulers())
                        .retry(),
                new RxLoaderObserver<Long>() {

                    @Override
                    public void onStarted() {
                        view.onWeatherLoadingStarted();
                    }

                    @Override
                    public void onNext(Long value) {}

                    @Override
                    public void onCompleted() {
                        view.onWeatherLoadingSuccess();
                        weatherLoader.clear();
                    }

                    @Override
                    public void onError(Throwable e) {
                        weatherLoader.clear();
                    }
                }).restart();
    }

    public void loadLocalWeather(){
        localWeatherLoader = loaderManager.create(LoaderTag.MAIN_LOCAl_WEATHER,
                DatabaseFactory.getProvider()
                        .provideCityWeatherSelectQuery()
                        .compose(RxUtils.<List<WeatherCity>>applySchedulers()),
                new RxLoaderObserver<List<WeatherCity>>() {

                    @Override
                    public void onNext(List<WeatherCity> cities) {
                        view.onLocalWeatherLoadingSuccess(cities);
                        localWeatherLoader.clear();
                    }

                    @Override
                    public void onError(Throwable e) {
                        localWeatherLoader.clear();
                    }
                }).restart();
    }



}
