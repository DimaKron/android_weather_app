package ru.example.weatherapp.api;

import android.support.annotation.NonNull;

public class ApiFactory {

    private static ApiProvider apiProvider;

    @NonNull
    public static ApiProvider getApiProvider(){
        return apiProvider;
    }

    public static void setApiProvider(@NonNull ApiProvider apiProvider) {
        ApiFactory.apiProvider = apiProvider;
    }

}
