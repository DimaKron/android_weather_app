package ru.example.weatherapp.api;


import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.example.weatherapp.model.entity.ForecastResponse;
import ru.example.weatherapp.model.entity.WeatherResponse;
import rx.Observable;

public interface WeatherService {

    @GET("data/2.5/group ")
    Observable<WeatherResponse> loadWeather(@Query("appid") String apiKey, @Query("id") String cityIds);

    @GET("data/2.5/forecast")
    Observable<ForecastResponse> loadForecast(@Query("appid") String apiKey, @Query("id") long cityId);
}
