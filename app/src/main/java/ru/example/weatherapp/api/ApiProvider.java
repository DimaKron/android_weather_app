package ru.example.weatherapp.api;

import android.support.annotation.NonNull;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiProvider {

    private WeatherService weatherService;

    public ApiProvider(@NonNull String baseUrl) {
        weatherService = createWeatherService(baseUrl);
    }

    @NonNull
    private WeatherService createWeatherService(@NonNull String baseUrl){
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(OkHttp.getClient())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WeatherService.class);
    }


    @NonNull
    public WeatherService provideWeatherService() {
        return weatherService;
    }
}
