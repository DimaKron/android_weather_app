package ru.example.weatherapp;

import android.app.Application;

import ru.example.weatherapp.BuildConfig;
import ru.example.weatherapp.api.ApiFactory;
import ru.example.weatherapp.api.ApiProvider;
import ru.example.weatherapp.database.DatabaseFactory;
import ru.example.weatherapp.database.DatabaseProvider;

public class AppDelegate extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ApiFactory.setApiProvider(new ApiProvider(BuildConfig.API_BASE_URL));

        DatabaseFactory.setProvider(new DatabaseProvider(getApplicationContext()));
    }

}
